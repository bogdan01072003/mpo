package org.example;

import java.util.Arrays;

public class Cells0 {
  private static int[] cells;
  private static int N; // кількість клітин
  private static int K; // кількість атомів
  private static double p; // ймовірність руху атома вправо

  // Клас, який представляє потік для кожного атома
  class AtomThread implements Runnable {
    private int position;

    public AtomThread(int startPos) {
      this.position = startPos; // початкова позиція атома
    }

    @Override
    public void run() {
      while (!Thread.currentThread().isInterrupted()) {
        double m = Math.random(); // Рухаємо атом вліво чи вправо в залежності від випадкового числа
        if (m > p) {
          if (position < N - 1) {
            cells[position]--; // зменшуємо кількість атомів в поточній клітинці
            cells[position + 1]++; // збільшуємо кількість атомів в сусідній клітинці
            position++; // оновлюємо позицію атома
          }
        } else {
          if (position > 0) {
            cells[position]--;
            cells[position - 1]++;
            position--;
          }
        }
        try {
          Thread.sleep(100);  // додано, щоб уповільнити виконання
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt(); // якщо потік було перервано, встановлюємо флаг переривання
        }
      }
    }
  }

  public static void main(String[] args) throws InterruptedException {
    N = 10;
    K = 5;
    p = 0.5;

    cells = new int[N];
    cells[0] = K; // спочатку всі атоми знаходяться в клітинці 0

    Thread[] threads = new Thread[K];
    Cells0 cellsObject = new Cells0();
    for (int i = 0; i < K; i++) {
      threads[i] = new Thread(cellsObject.new AtomThread(0)); // створюємо поток для кожного атома
      threads[i].start(); // запускаємо потік
    }

    // Створення потоку для виводу стану масиву клітин кожну секунду
    Thread logger = new Thread(() -> {
      try {
        while (!Thread.currentThread().isInterrupted()) {
          Thread.sleep(1000); // Чекаємо 1 секунду
          synchronized (cells) {
            // Виведення стану масиву
            System.out.println("Cells: " + Arrays.toString(cells));
          }
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    });

    logger.start(); // Запуск потоку для логування

    Thread.sleep(10000);  // 10 секунд

    for (Thread thread : threads) {
      thread.interrupt(); // перериваємо всі потоки-атоми
    }

    logger.interrupt(); // Перериваємо потік логування

    int sum = 0;
    for (int cell : cells) {
      sum += cell; // рахуємо загальну кількість атомів
    }
    System.out.println("Total number of atoms: " + sum);
  }
}
