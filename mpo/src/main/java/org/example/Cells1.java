package org.example;

import java.util.Arrays;

import java.util.Arrays;

public class Cells1 {
  private static int[] cells; // Масив клітин, в яких зберігаються атоми

  // Клас, який представляє потік для кожного атома
  class AtomThread implements Runnable {
    private int position; // Поточна позиція атома

    public AtomThread(int startPos) {
      this.position = startPos; // Ініціалізація початкової позиції атома
    }

    @Override
    public void run() {
      while (!Thread.currentThread().isInterrupted()) {
        synchronized (cells) { //  весь код в цьому блоку буде виконуватися тільки одним потоком одночасно
          double m = Math.random(); // Рухаємо атом вліво чи вправо в залежності від випадкового числа
          if (m > p) {
            if (position < N - 1) {
              cells[position]--; // Зменшуємо кількість атомів в поточній клітинці
              cells[position + 1]++; // Збільшуємо кількість атомів в наступній клітинці
              position++; // Оновлюємо позицію атома
            }
          } else {
            if (position > 0) {
              cells[position]--; // Зменшуємо кількість атомів в поточній клітинці
              cells[position - 1]++; // Збільшуємо кількість атомів в попередній клітинці
              position--; // Оновлюємо позицію атома
            }
          }
        }
        try {
          Thread.sleep(100);  // додано, щоб уповільнити виконання
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt(); // Встановлюємо флаг переривання, якщо потік було перервано
        }
      }
    }
  }

  private static int N; // Кількість клітин
  private static int K; // Загальна кількість атомів
  private static double p; // Ймовірність руху атома вправо

  public static void main(String[] args) throws InterruptedException {
    N = 50;
    K = 30;
    p = 0.5;

    cells = new int[N];
    cells[0] = K; // Спочатку всі атоми знаходяться в першій клітинці

    Thread[] threads = new Thread[K];
    Cells1 cellsObject = new Cells1();
    for (int i = 0; i < K; i++) {
      threads[i] = new Thread(cellsObject.new AtomThread(0));
      threads[i].start();
    }

    // Створення потоку для виводу стану масиву клітин кожну секунду
    Thread logger = new Thread(() -> {
      try {
        while (!Thread.currentThread().isInterrupted()) {
          Thread.sleep(5000); // Чекаємо 1 секунду
          synchronized (cells) {
            // Виведення стану масиву
            System.out.println("Cells: " + Arrays.toString(cells));
          }
        }
      } catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    });

    logger.start(); // Запуск потоку для логування

    Thread.sleep(60000); // 10 секунд

    for (Thread thread : threads) {
      thread.interrupt(); // Перериваємо роботу потоків-атомів
    }

    logger.interrupt(); // Перериваємо потік логування

    // Підсумовуємо результат
    int sum = 0;
    synchronized (cells) {
      for (int cell : cells) {
        sum += cell; // Рахуємо загальну кількість атомів у всіх клітинках
      }
    }
    System.out.println("Total number of atoms: " + sum);
  }
}
