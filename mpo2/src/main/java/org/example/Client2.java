package org.example;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.*;

public class Client2 {

  private static final String SERVER_ADDRESS = "localhost";
  private static final int SERVER_PORT = 1232;
  private static JTextArea textArea;
  private static JTextField inputField;
  private static PrintWriter out;
  private static boolean enteredName = false;
  private static boolean explainedPrivateMessage = false;

  public static void main(String[] args) {
    SwingUtilities.invokeLater(() -> {
      JFrame frame = new JFrame("Chat Client");
      setupGUI(frame);

      inputField.requestFocusInWindow();

      inputField.setEnabled(true);

      inputField.addActionListener(e -> {
        if (!enteredName) {
          String name = inputField.getText();
          sendMessage(name);
          inputField.setText("");
          enteredName = true;
        } else {
          String message = inputField.getText();
          sendMessage(message);
          inputField.setText("");
        }
      });

      frame.setSize(400, 300);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);

      if (!enteredName) {
        askForName();
      }
    });

    connectToServer();
  }

  private static void setupGUI(JFrame frame) {
    JPanel panel = new JPanel();
    panel.setLayout(new BorderLayout());

    textArea = new JTextArea(10, 40);
    textArea.setEditable(false);
    JScrollPane scrollPane = new JScrollPane(textArea);
    panel.add(scrollPane, BorderLayout.CENTER);

    JPanel inputPanel = new JPanel();
    inputPanel.setLayout(new BorderLayout());

    inputField = new JTextField();
    inputField.setPreferredSize(new Dimension(300, 30));
    inputPanel.add(inputField, BorderLayout.CENTER);

    JButton sendButton = new JButton("Send");
    sendButton.setPreferredSize(new Dimension(80, 30));
    sendButton.addActionListener(e -> {
      if (!enteredName) {
        String name = inputField.getText();
        sendMessage(name);
        inputField.setText("");
        enteredName = true;
      } else {
        String message = inputField.getText();
        sendMessage(message);
        inputField.setText("");
      }
    });
    inputPanel.add(sendButton, BorderLayout.EAST);

    panel.add(inputPanel, BorderLayout.SOUTH);
    frame.add(panel);
  }

  private static void askForName() {
    textArea.setText("Please enter your name:");
  }

  private static void connectToServer() {
    try {
      Socket socket = new Socket(SERVER_ADDRESS, SERVER_PORT);
      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      out = new PrintWriter(socket.getOutputStream(), true);

      String serverMessage;
      while ((serverMessage = in.readLine()) != null) {
        final String message = serverMessage;
        SwingUtilities.invokeLater(() -> {
          if (!explainedPrivateMessage && enteredName) {
            textArea.append(message + "\n\nTo send a private message, use: private: (person name) (message)\n\n");
            explainedPrivateMessage = true;
          } else {
            textArea.append(message + "\n");
          }
        });
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void sendMessage(String message) {
    out.println(message);
  }
}


