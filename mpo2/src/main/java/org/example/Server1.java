package org.example;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Server1 {

  private static final int PORT = 1232;
  private static final Map<String, ClientHandler> clientHandlers = new ConcurrentHashMap<>();
  private static final JTextArea textArea = new JTextArea();
  private static ServerSocket serverSocket;

  public static void main(String[] args) {
    setupGUI();
    startServer();
  }

  private static void setupGUI() {
    SwingUtilities.invokeLater(() -> {
      JFrame frame = new JFrame("Server");
      textArea.setEditable(false);
      textArea.setForeground(Color.LIGHT_GRAY); // Змінено колір тексту
      textArea.setBackground(Color.BLACK); // Змінено колір фону
      Font font = new Font("Arial", Font.PLAIN, 15); // Змінено шрифт
      textArea.setFont(font);

      frame.add(new JScrollPane(textArea), BorderLayout.CENTER);
      frame.setSize(500, 500);
      frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      frame.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
          closeServer();
        }
      });
      frame.setVisible(true);
    });
  }

  private static void startServer() {
    try {
      serverSocket = new ServerSocket(PORT);
      System.out.println("Server");
      SwingUtilities.invokeLater(() -> textArea.append("Server is running...\n"));
      while (true) {
        Socket clientSocket = serverSocket.accept();
        ClientHandler clientHandler = new ClientHandler(clientSocket);
        new Thread(clientHandler).start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void closeServer() {
    try {
      if (serverSocket != null && !serverSocket.isClosed()) {
        serverSocket.close();
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    System.exit(0);
  }

  private static class ClientHandler implements Runnable {
    private Socket socket;
    private String username;
    private BufferedReader in;
    private PrintWriter out;

    public ClientHandler(Socket socket) {
      this.socket = socket;
    }

    @Override
    public void run() {
      try {
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        // Register username
        username = in.readLine();
        sendMessage("\nYour name - " + username);  // Add this line
        clientHandlers.put(username, this);
        SwingUtilities.invokeLater(() -> textArea.append(username + " joi ned the chat.\n"));

        // Process messages
        String message;
        while ((message = in.readLine()) != null) {
          final String finalUsername = username;
          final String finalMessage = message;
          if (message.startsWith("private:")) {
            processPrivateMessage(message);
          } else {
            processPublicMessage(finalUsername, finalMessage);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      } finally {
        closeSocket();
        removeClientHandler();
      }
    }

    private void processPrivateMessage(String message) {
      String[] tokens = message.split(" ", 3);
      if (tokens.length == 3) {
        String recipient = tokens[1];
        String privateMessage = tokens[2];
        ClientHandler recipientHandler = clientHandlers.get(recipient);
        if (recipientHandler != null) {
          recipientHandler.sendMessage(username + " (private): " + privateMessage);
        } else {
          sendMessage("Client " + recipient + " not found.");
        }
      } else {
        sendMessage("Invalid private message format.");
      }
    }

    private void processPublicMessage(String finalUsername, String finalMessage) {
      SwingUtilities.invokeLater(() -> textArea.append(finalUsername + ": " + finalMessage + "\n"));
      for (ClientHandler handler : clientHandlers.values()) {
        handler.sendMessage(finalUsername + ": " + finalMessage);
      }
    }

    private void closeSocket() {
      try {
        socket.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    private void removeClientHandler() {
      clientHandlers.remove(username);
      SwingUtilities.invokeLater(() -> textArea.append(username + " left the chat.\n"));
    }

    private void sendMessage(String message) {
      out.println(message);
    }
  }
}
